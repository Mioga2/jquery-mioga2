// ========================================================
// jquery.foldable.js
// A foldable box plugin
// Usage :
//     <div class="sbox">
//         <h2 class="title_bg_color">Some title</h2>
//         <div class="content"></div>
//     </div>
//
//     $('div.sbox').foldable ();
// ========================================================
(function ($) {
	$.fn.foldable = function () {
		this.each (function (i, item) {
			var $elem = $(item);

			// Add status-icon container
			$elem.find ('h2:first').append ($('<span class="fold-icon">&#160;</span>')).addClass ('foldable');

			// Enable sbox folding
			$elem.find ('span.fold-icon').click (function () {
				$(this).parent ().parent().find ('div.content:first').slideToggle (function () {
					$(this).parent ().find ('h2:first span.fold-icon').toggleClass ('unfold');
				});
			});
		});

		return (this);
	};
}) (jQuery);


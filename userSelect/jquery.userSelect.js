/**
@name jquery.userSelect
@type jQuery
@author developers@mioga2.org

@requires jQuery v1.7.1
@requires jquery-ui.js v1.8.16
@requires jquery.ba-resize.js
@requires jquery.tablesorter.js v-2.5.2
@requires jquery.tablesorter.widgets.js
@requires jquery.tableScrollableSortable.js

style sheet :
				jquery.tableScrollableSortable.css
				jquery.userSelect.css

Copyright 2012, (http://alixen.fr)
The Mioga2 Project. All Rights Reserved.
This Plugin is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

@description 	This Plugin shows users list that given to Plugin. It can select or select none each user and apply action to the user selection. It can sort user list by each header of table.
				note : if using this plugin in Jquery dialog in Jquery Tabs, it is necessary to initialize 1) dialog 2) userSelect and 3) tabs.

@param Object
	settings An object literal containing key/value pairs to provide optional settings.
		@option String app_name that is the name of current application.
		@option Object preferences containing preferences of columns table dimensions and default columns of table sorter. It contains key value pair like :
				@option Integer width that is reference to total width of table.
				@option Array sortList that containing array of two integer to defined default columns sorter.
						The first array is a priority column. 
						The first integer is the columns index and second integer is a asc or desc sorting.
				@option Array field_column that containing objects with key "perc" and value as integer. It is a width proportion versus "width" key.
		@option Function storePreferences that is callback to save preferences. That is called when table is resized or sorted.
		@option data can be Array or Function.
				If it s an array, it must be containing user list with minimum keys : "rowid" as integer, "firstname" as String, "lastname" as String, "email" as String.
				If it s a function, it must returns an array of user or integer value. If it return integer, this means that count user is too much and this integer is count of user.
				This Plugin call data with arguments like : {app_name:string_value,lastname:"A*"} and load the users whose name starts with "a".
		@option Object i18n that containing translate data. See example for default values
		@option Boolean single that determines if multiple or one user can be selected. 
		@option Function select_add that is callback called on validate button is clicked.
				@param Array of selected users id
		@option select_change that is callback called when user selection is modified.
				@param Integer id that is user id.
				@param Boolean that is a new user status, selected or not.

@example 

	var options = {
			app_name : "Haussmann",
			preferences : {
				width : 100,
			     sortList : [ [0,1], [1,1] ],
			     field_column : [
			           {
			        	   perc : 10
			           },
			           {
			        	   perc : 30
			           },
			           {
			        	   perc : 10
			           },
			           {
			        	   perc : 50
			           }
			     ]
			},
			storePreferences : function (prefs) {
				console.log(prefs);
			},
			single : false,
			data	: 
				function (arg) {
				var data;
				if (arg) {
					var new_list = new Array();
					$.each(list1, function (i,e) {
						if (e.lastname.substr(0,1).toUpperCase() === arg.lastname.substr(0,1).toUpperCase()) {
							new_list.push(e);
						}
					});
					data = new_list;
				}
				else {
					if (id_load_btn === "load_us_too") {
						data = 6;
					}
					else if (id_load_btn === "load_us") {
						data = new Array();
						for (var i=0; i<30; i++) {
							data.push({
								rowid 		: i,
								firstname 	: "toto_" + i,
								lastname  	: "titi_" + i,
								email	  	: "toto_" + i + "titi_" + i + "@alixen.fr"
							});
						}
					}
				}
				return data;
			},
			i18n : {
				 select_all		: "Select all",
				 deselect_all 	: "Select none",
				 add_btn		: "Add",
				 columns : [
						{
							field: 'firstname',
							label: 'Firstname'
						},
						{
							field: 'lastname',
							label: 'Lastname',
							Default: true		// apply this preference if key "preferences" is not defined.
						},
						{
							field: 'email',
							label: 'E-mail'
						}
				 ]
			},
			select_add : function (rowids) {
				var userList = $('#us-cont-id').userselect('get_list');
				$.each(userList, function (i,e) {
					if (e.selected === true) {
						$('#users_added ul').append('<li class="us-item">id user added : ' + e.rowid + '</li>');
					}
				});
				$('#us-cont-id').userselect('refresh_list');
			},
			select_change : function (id, selected) {
			}
	};

	$('#id-cont').userSelect(options);
**/
(function( $ ){
	var defaults = {
			app_name : undefined,
			preferences : undefined,
			storePreferences : undefined,
			data : [],
			i18n : {
				select_all		: "Select all",
				deselect_all 	: "Select none",
				add_btn		: "Add",
				columns : [
						{
							field: 'firstname',
							label: 'Firstname'
						},
						{
							field: 'lastname',
							label: 'Lastname',
							Default: true
						},
						{
							field: 'email',
							label: 'E-mail'
						}
				]
			},
			single : false,
			select_add : undefined,
			select_change : undefined
	};
	// ==================================================
	// ========= privates methods =======================
	// ==================================================
	/**
	@description This function activates or deactivates "select all" and "select none" buttons according to user existence.
				 It activates or desactivates "validate" button according to selected user or not.
	@method activateBtn
	@param Jquery selector of Plugin.
	**/
	function activateBtn ($elem) {
		if ($elem.find('tr.mioga2-userselect-user-cont').length === 0) {
			$elem.find('.mioga2-userselect-select_all, .mioga2-userselect-select_none').attr('disabled', true).removeClass('disabled').addClass('disabled');
		}
		else {
			$elem.find('.mioga2-userselect-select_all, .mioga2-userselect-select_none').removeAttr('disabled').removeClass('disabled');
		}
		if ($('td.mioga2-userselect-selected').length === 0) {
			$elem.find('.mioga2-userselect-close-btn').removeAttr('disabled').removeClass('disabled');
			$elem.find('.mioga2-userselect-add-btn').attr('disabled', true).removeClass('disabled').addClass('disabled');
		}
		else {
			$elem.find('.mioga2-userselect-close-btn').attr('disabled', true).removeClass('disabled').addClass('disabled');
			$elem.find('.mioga2-userselect-add-btn').removeAttr('disabled').removeClass('disabled');
		}
	}
	/**
	@description This function refresh table
	@method refreshTable
	@param Jquery selector of Plugin.
	**/
	function refreshTable ($elem) {
		$elem.find('.mioga2-userselect-table').trigger("update").trigger("appendCache");
		$elem.find('.mioga2-userselect-list-cont').tableScrollableSortable('refresh');
	}
	/**
	@description This function sorts user list according to first char of lastname
	@method sortData
	@param Array user_list
	@param String char is a character
	**/
	function sortData(user_list, char) {
		var new_data_l = user_list.length;
		var new_data_list = [];
		if (char.length && char !== "*") {
			for (var k=0; k < new_data_l; k++) {
				if (user_list[k].lastname.substr(0,1).toLowerCase() === char.toLowerCase()) {
					new_data_list.push(user_list[k]);
				}
			}
		}
		else if (char === "*"){
			var letters = /^[a-zA-Z]+$/;
			for (var i=0; i < new_data_l; i++) {
				if (letters.test(user_list[i].lastname.substr(0,1)) === false ) {
					new_data_list.push(user_list[i]);
				}
			}
		}
		else {
			new_data_list = user_list;
		}
		return new_data_list;
	}
	/**
	@description This function remove all HTML user item and redraw user list in table.
	@method drawUser
	@param Jquery selector of Plugin.
	@param Array that containing user list.
	@param Array that containing columns name translation.
	**/
	function drawUser($elem, data, i18n_columns, $item) {
		var $height = $elem.innerHeight() - $elem.css ('paddingTop').replace ('px', '') - $elem.css ('paddingBottom').replace ('px', '');
		$elem.height($height - parseInt($elem.css('border-top-width')) + parseInt($elem.css('border-bottom-width')));
		
		var height_util = $height;
		var $tbl_cont = $elem.find('.mioga2-userselect-cont');
		$.each($tbl_cont.children(), function (i,e) {
			if ($(e).hasClass('mioga2-userselect-list-cont') === false) {
				height_util -= $(e).outerHeight( true );
			}
		});
		$elem.find('.mioga2-userselect-list-cont').height(height_util + "px");
		
		var $ruler = $elem.find('.mioga2-userselect-ruler-cont');
		var new_data;
		var new_data_list = [];
		if ($.isArray(data) === true) {
			new_data = data;
			if ($item === undefined) {
				new_data_list = data;
				$ruler.find ('.ui-state-active').removeClass('ui-state-active');
				$ruler.find ('#mioga2-userselect-ruler-radio-all').next('label').addClass('ui-state-active');
			}
			else {
				new_data_list = sortData(new_data, $item.val() );
			}
		}
		else if ($.isFunction(data) === true) {
			if ($item === undefined) {
				new_data = data();
				if ($.isNumeric(new_data) === true) {
					$ruler.find ('.ui-state-active').removeClass('ui-state-active');
					$ruler.find ('#mioga2-userselect-ruler-radio-A').next('label').addClass('ui-state-active');
					new_data_list = data ({
						app_name	: $elem.data('mioga2_userselect_params').app_name,
						lastname 	: "A*"
					});
				}
				else {
					new_data_list = new_data;
				}
			}
			else {
				var args = {
					app_name	: $elem.data('mioga2_userselect_params').app_name
				};
				if ($item.val().length) {
					args.lastname = $item.val() + "*";
				}
				new_data = data (args);
				new_data_list = sortData(new_data, $item.val() );
			}
		}
		else {
			result = false;
			$.error('userSelect - data key must be a function or an array.');
		}
		
		$elem.find('.mioga2-userselect-user-cont').remove();
		
		$.each(new_data_list, function (i,e) {
			var $user_cont = $('<tr class="mioga2-userselect-user-cont"><td class="mioga2-userselect-user-selected" >a</td></tr>');
			var user_info = {rowid : e.rowid, selected : false};
			$.each (i18n_columns, function (ind,key) {
				user_info[key.field] = e[key.field];
				$user_cont.append('<td class="mioga2-userselect-item-' + key.field + '">' + e[key.field] + '</td>');
			});
			$user_cont.data('user_data', user_info);
			$elem.find('.mioga2-userselect-table').append($user_cont);
		});
		activateBtn($elem);
	}
	/**
	@description Public method that called on plugin initialization.
				 It draws HTML containers and buttons, and apply behavior to each element.
				 It draws data and initializes "tableScrollableSortable" plugin.
	@method init
	@param Object containing plugin options extends with default values of plugin.
	**/
	var methods = {
		init : function(args) {
			var base_obj = $.extend({}, defaults, args);

			return $(this).each(function() {
				var $this = $(this);
				var $height = $this.innerHeight() - $this.css ('paddingTop').replace ('px', '') - $this.css ('paddingBottom').replace ('px', '');
				$this.height($height - parseInt($this.css('border-top-width')) + parseInt($this.css('border-bottom-width')));
				$this.data("mioga2_userselect_params", base_obj);
				// Draw table container
				$this.children().remove();
				var $tbl_cont = $('<div class="mioga2-userselect-cont"></div>');
				var $ruler = $(
					'<div class="mioga2-userselect-ruler-cont">'
						+ '<input type="radio" name="ruler-link" value="" id="mioga2-userselect-ruler-radio-all"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-all" class="ui-state-active">All</label></input>'
						+ '<input type="radio" name="ruler-link" value="A" id="mioga2-userselect-ruler-radio-A"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-A">A</label></input>'
						+ '<input type="radio" name="ruler-link" value="B" id="mioga2-userselect-ruler-radio-B"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-B">B</label></input>'
						+ '<input type="radio" name="ruler-link" value="C" id="mioga2-userselect-ruler-radio-C"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-C">C</label></input>'
						+ '<input type="radio" name="ruler-link" value="D" id="mioga2-userselect-ruler-radio-D"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-D">D</label></input>'
						+ '<input type="radio" name="ruler-link" value="E" id="mioga2-userselect-ruler-radio-E"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-E">E</label></input>'
						+ '<input type="radio" name="ruler-link" value="F" id="mioga2-userselect-ruler-radio-F"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-F">F</label></input>'
						+ '<input type="radio" name="ruler-link" value="G" id="mioga2-userselect-ruler-radio-G"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-G">G</label></input>'
						+ '<input type="radio" name="ruler-link" value="H" id="mioga2-userselect-ruler-radio-H"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-H">H</label></input>'
						+ '<input type="radio" name="ruler-link" value="I" id="mioga2-userselect-ruler-radio-I"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-I">I</label></input>'
						+ '<input type="radio" name="ruler-link" value="J" id="mioga2-userselect-ruler-radio-J"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-J">J</label></input>'
						+ '<input type="radio" name="ruler-link" value="K" id="mioga2-userselect-ruler-radio-K"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-K">K</label></input>'
						+ '<input type="radio" name="ruler-link" value="L" id="mioga2-userselect-ruler-radio-L"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-L">L</label></input>'
						+ '<input type="radio" name="ruler-link" value="M" id="mioga2-userselect-ruler-radio-M"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-M">M</label></input>'
						+ '<input type="radio" name="ruler-link" value="N" id="mioga2-userselect-ruler-radio-N"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-N">N</label></input>'
						+ '<input type="radio" name="ruler-link" value="O" id="mioga2-userselect-ruler-radio-O"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-O">O</label></input>'
						+ '<input type="radio" name="ruler-link" value="P" id="mioga2-userselect-ruler-radio-P"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-P">P</label></input>'
						+ '<input type="radio" name="ruler-link" value="Q" id="mioga2-userselect-ruler-radio-Q"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-Q">Q</label></input>'
						+ '<input type="radio" name="ruler-link" value="R" id="mioga2-userselect-ruler-radio-R"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-R">R</label></input>'
						+ '<input type="radio" name="ruler-link" value="S" id="mioga2-userselect-ruler-radio-S"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-S">S</label></input>'
						+ '<input type="radio" name="ruler-link" value="T" id="mioga2-userselect-ruler-radio-T"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-T">T</label></input>'
						+ '<input type="radio" name="ruler-link" value="U" id="mioga2-userselect-ruler-radio-U"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-U">U</label></input>'
						+ '<input type="radio" name="ruler-link" value="V" id="mioga2-userselect-ruler-radio-V"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-V">V</label></input>'
						+ '<input type="radio" name="ruler-link" value="W" id="mioga2-userselect-ruler-radio-W"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-W">W</label></input>'
						+ '<input type="radio" name="ruler-link" value="X" id="mioga2-userselect-ruler-radio-X"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-X">X</label></input>'
						+ '<input type="radio" name="ruler-link" value="Y" id="mioga2-userselect-ruler-radio-Y"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-Y">Y</label></input>'
						+ '<input type="radio" name="ruler-link" value="Z" id="mioga2-userselect-ruler-radio-Z"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-Z">Z</label></input>'
						+ '<input type="radio" name="ruler-link" value="*" id="mioga2-userselect-ruler-radio-*"   class="mioga2-userselect-ruler-link"><label for="mioga2-userselect-ruler-radio-*">*</label></input>'
					+ '</div>'
				).buttonset ();
				var $glob_selection = $(
					'<div class="mioga2-userselect-link-cont">'
						+ '<input type="button" class="button mioga2-userselect-select_all" value="' + base_obj.i18n.select_all   + '"/>'
						+ '<input type="button" class="button mioga2-userselect-select_none" value="' + base_obj.i18n.deselect_all + '"/>'
					+ '</div>'
				);
				$tbl_cont.append($ruler);
				$tbl_cont.append(
					'<div class="mioga2-userselect-list-cont">'
						+ '<table class="mioga2-userselect-table list tablesorter">'
							+ '<thead>'
								+ '<tr class="mioga2-userselect-label-cont">'
									+ '<th class="mioga2-userselect-selected-lbl"></th>'
								+ '</tr>'
							+ '</thead>'
							+ '<tbody></tbody>'
						+ '</table>'
					+ '</div>'
					+ '<div class="mioga2-userselect-actions-cont">'
						+ '<input type="button" class="button mioga2-userselect-add-btn disabled" value="' + base_obj.i18n.add_btn + '"/>'
					+ '</div>'
				);
				$this.append($tbl_cont);
				function eventClickRuler ($item) {
					var current_data = $this.data("mioga2_userselect_params").data;
					drawUser($this,current_data, base_obj.i18n.columns, $item);
					refreshTable ($this);
				}
				
				$.each($ruler.find('input'), function (i,link) {
					$(this).click(function (event) {
						eventClickRuler($(this));
					});
				});
				
				if (base_obj.single === false) {
					$tbl_cont.prepend($glob_selection);
					// behavior to select or select none users
					$this.find('.mioga2-userselect-select_all').click(function () {
						$this.find('.mioga2-userselect-user-selected').removeClass('mioga2-userselect-selected').addClass('mioga2-userselect-selected');
						activateBtn($this);
					});
					$this.find('.mioga2-userselect-select_none').click(function () {
						$this.find('.mioga2-userselect-user-selected').removeClass('mioga2-userselect-selected');
						activateBtn($this);
					});
				}
				drawUser($this,base_obj.data, base_obj.i18n.columns);
				// determine height of table container
				var height_util = $height;
				$.each($tbl_cont.children(), function (i,e) {
					if ($(e).hasClass('mioga2-userselect-list-cont') === false) {
						height_util -= $(e).outerHeight( true );
					}
				});
				$this.find('.mioga2-userselect-list-cont').height(height_util + "px");
				$this.find('.mioga2-userselect-label-cont').data({i18n_columns: base_obj.i18n.columns});
				
				// Draw table header
				$.each(base_obj.i18n.columns, function (i,e) {
					var $header_item = $('<th class="mioga2-userselect-label-' + e.field + '">' + e.label + '</th>').data({'mioga2_userselect_label' : e});
					$this.find('.mioga2-userselect-label-cont').append($header_item);
				});
				// behavior to select or deselect item, and disabled or not if item selected
				$this.find('tbody').delegate('.mioga2-userselect-user-cont', 'click',function(event) {
					var id = $(this).data('user_data').rowid;
					var $selected = $(this).find('.mioga2-userselect-user-selected');
					var selected;
					if ($(this).data('user_data').selected) {
						$(this).data('user_data').selected = false;
						selected = false;
						$selected.removeClass('mioga2-userselect-selected').text('');
					}
					else {
						if (base_obj.single === true) {
							$.each($this.find('.mioga2-userselect-user-cont'), function (i,e) {
								$(e).data('user_data').selected = false;
								$(e).find('.mioga2-userselect-selected').removeClass('mioga2-userselect-selected').text('unselected');
							});
						}
						selected = true;
						$(this).data('user_data').selected = true;
						$selected.addClass('mioga2-userselect-selected').text('selected');
					}
					$this.find('.mioga2-userselect-table').trigger("update").trigger("appendCache");
					activateBtn($this);
					if ($.isFunction(base_obj.select_change)) {
						base_obj.select_change(id, selected);	
					}
				});
				// behavior to add selection
				$this.find('.mioga2-userselect-add-btn').live('click',function(event) {
					if ($.isFunction(base_obj.select_add)) {
						var rowids = [];
						$.each($this.find('.mioga2-userselect-selected'), function (i,e) {
							rowids.push($(e).parent().data('user_data').rowid);
						});
						base_obj.select_add(rowids);
					}
				});
				// determine the column that will be default sorting
				var column_index;
				$.each($this.find('.mioga2-userselect-label-cont').find('th'), function (i,e) {
					if ($(this).data('mioga2_userselect_label')) {
						if ($(this).data('mioga2_userselect_label').Default) {
							column_index = $(this).index();
						}
					}
				});
				var sort_options = {
					sortList	: [[column_index,0]],
					widgets: [ "resizable" ] ,
					widgetOptions : { },
					widthFixed: false
				};
				
				$this.find('.mioga2-userselect-list-cont').tableScrollableSortable({
					sortable : {
						sorterOpt : sort_options,
						preferences : base_obj.preferences,
						storePreferences : base_obj.storePreferences 
					}
				});
			});
		},
		/**
		@description Public method that replaces user list.
		@method replace_list
		@param Object args is like data paramters of plugin.
		**/
		replace_list	: function (args) {
			return $(this).each(function() {
				var $this 	= $(this);
				$this.data("mioga2_userselect_params").data = args;
				var i18n_columns = $this.find('.mioga2-userselect-label-cont').data('i18n_columns');
				drawUser($this, args, i18n_columns);
				refreshTable ($this);
			});
		},
		/**
		@description Public method that removes all selected items.
		@method refresh_list
		@param none
		**/
		refresh_list	: function  () {
			return $(this).each(function() {
				var $this = $(this);
				$this.find('.mioga2-userselect-selected').parent().remove();
				activateBtn($this);
			});
		},
		/**
		@description Public method that inserts annotations for each user containing in parameter.
		@method annotate_list
		@param Array that contains key value pair as : "rowid" containing user id, "message" containing annotation as String. 
		**/
		annotate_list	: function  (args) {
			var msg_list = args;
			return $(this).each(function () {
				var $this = $(this);
				$.each(msg_list, function (i,e) {
					$.each($this.find('.mioga2-userselect-user-cont'), function (ind,elm) {
						if ($(elm).data("user_data").rowid === e.rowid) {
							$(elm).find('.mioga2-userselect-annoted').remove();
							$(elm).find('td:last').append('<span class="mioga2-userselect-annoted">' + e.message + '</span>');
							return;
						}
					});
				});
			});
		},
		/**
		@description Public method that returns array of selected user Object as : key : rowid, key selected as boolean.
					 
		@method get_list
		@param none. 
		**/
		get_list	: function () {
			var $this = $(this);
			var current_list = [];
			$.each($this.find('.mioga2-userselect-user-cont'), function (i,e) {
				current_list.push($(e).data('user_data'));
			});
			return current_list;
		}
};
	/**
	@namespace $.fn.userSelect
	**/
	$.fn.userSelect = function(method) {
		var args = arguments;
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
		}
		else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, args );
		}
		else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.userSelect' );
		}
	};
})( jQuery );

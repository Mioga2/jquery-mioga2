/**
@name jquery.fileBrowser
@type jQuery
@author developers@mioga2.org

@requires jQuery v1.7.1
@requires jquery.dav.js
@requires xml2json.js
@requires jquery.fileBrowser.css

Copyright 2012, (http://alixen.fr)
The Mioga2 Project. All Rights Reserved.
This Plugin is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

@description fileBrowser Plugin draw HTML block containing directory of DAV folder and content of this current folder. It can navigate in DAV tree directory.

@param Object
	settings An object literal containing key/value pairs to provide optional settings.
		@option String root that is root of DAV directory
		@option String current that is current directory or selected file
		@option Function change_directory that is a callback called when current directory is modified or file is selected.


@example $('#id-cont').filebrowser({
	root : "/dav/root/folder/",
	current : "My/home/project1/"
	change_directory : function () {
		console.log("current directory is modified.");
	}
});

TODO
	It must be defined just css background for mimestype icon. For example if mimetype is "text/plain", adjust CSS class like : 

	.mioga2-filebrowser-mimetype-text\/plain {
		background: url("../images/16x16/mimetypes/application-vnd.oasis.opendocument.text.png") no-repeat scroll left;
	}
**/

(function( $ ){
	var defaults = {
		root 	: undefined,
		current	: "",
		change_directory: undefined
	};
	// ==================================================
	// ========= privates methods =======================
	// ==================================================
	/**
	@description Private method refreshList.
				 Method that retrieves content of folder by webdav request and refresh draw.

	@method refreshList
	@param Jquery selector of Plugin
	@param String root
	@param String current
	@param Function change_directory
	**/
	function refreshList ($this, root, current, change_directory) {
		var pathDav = root + "";
		if (current !== "") {
			pathDav = pathDav + '/' + current;
		}
		pathDav = pathDav.replace(/\?/g, "%3f");
		jQuery.Dav(pathDav + "/").readFolder({
			async: false,
			success: function(dat, stat,response) {
				function getFolderName (o) {
					var n = o.href;
					if (n.charAt(n.length-1) === "/") {
						n = n.substr(0, n.length-1);
					}
					n = decodeURIComponent (n.substr(n.lastIndexOf('/')+1));
					return n;
				}
				refreshDirectory($this, root, current);
				if (change_directory instanceof Function) {
					change_directory(pathDav);
				}
				var $list = $this.find('.mioga2-filebrowser-list');
				$list.children().remove();

				var json_tmp 	= $.xml2json(dat);
				var str_tmp 	= JSON.stringify(json_tmp);
				str_tmp 		= str_tmp.replace(/Mioga:/g, "").replace(/D:/g, "");
				var t 			= $.parseJSON(str_tmp);
				var arrayFolder = [];
				var arrayFile = [];
				$.each(t.response, function (ind, elem) {
					if (elem.propstat) {
						if (elem.propstat.prop) {
							if (elem.propstat.prop.mimetype) {
								if (elem.propstat.prop.mimetype === "directory") {
									arrayFolder.push(elem);
								}
								else {
									arrayFile.push(elem);
								}
							}
						}
					}
				});
				arrayFolder.sort(function(a,b){
					if (getFolderName(a) > getFolderName(b)) {
						return 1;
					}
					else {
						return -1;
					}
				});
				arrayFile.sort(function(a,b){
					if (decodeURIComponent (a.href.substring(a.href.lastIndexOf('/') + 1)) > decodeURIComponent (b.href.substring(b.href.lastIndexOf('/') + 1))) {
						return 1;
					}
					else {
						return -1;
					}
				});
				
				$.each(arrayFolder, function (i,e) {
					var mimetype = e.propstat.prop.mimetype;
					var name = getFolderName(e);

					$('<li class="mioga2-filebrowser-item"><span class="mioga2-filebrowser-mimetype-' + mimetype + '"></span><a href="#" class="mioga2-filebrowser-item-name mioga2-filebrowser-dir-link">' + name + '</a></li>')
						.data({
							fullpath	: cleanupPath (root + '/' + current + '/' + name),
							relpath		: cleanupPath (current + '/' + name, true),
							name 		: name,
							mimetype 	: mimetype,
							current 	: current
						})
						.appendTo($list);
				});
				$.each(arrayFile, function (i,e) {
					var name 	 = decodeURIComponent (e.href.substring(e.href.lastIndexOf('/') + 1));
					var mimetype = e.propstat.prop.mimetype;
					$('<li class="mioga2-filebrowser-item"><span class="mioga2-filebrowser-mimetype-' + mimetype + '"></span><a class="mioga2-filebrowser-item-name" href="' + pathDav + "/" + name + '" target="_blank">' + name + '</a></li>')
						.data({
							fullpath	: cleanupPath (root + '/' + current + '/' + name),
							relpath		: cleanupPath (current + '/' + name, true),
							name 		: name,
							mimetype 	: mimetype,
							current 	: current
						})
						.appendTo($list);
				});
			},
			error : function () {
				if (current != "") {
					current = "";
					alert('Directory not found. Go at home. If this problem is persistent, contact your Mioga2 administrator.');
					refreshList ($this, root, current, change_directory);
				}
				else {
					alert('bad directory !');
				}
			}
		});
	}
	/**
	@description Private method refreshDirectory.
				 Method that redraws directory

	@method refreshDirectory
	@param Jquery selector of Plugin
	@param String root
	@param String current
	**/
	// Function thats redraws block of directory links
	function refreshDirectory ($this,root, current) {
		var $cont_dir = $this.find('.mioga2-filebrowser-directory p');
		$cont_dir.children().remove();
		var arrayCurrent = [];
		var current_root = root.substr(0, root.length-2);
		current_root = root.substr(root.lastIndexOf('/'));
		var dir_w = 0;
		if (current === "" ) {
			$cont_dir.append('<span></span><span>' + current_root.substr(1) + '</span>');
		}
		else {
			$cont_dir.append('<span>/</span><a href="#" class="mioga2-filebrowser-dir-link">' + current_root.substr(1) + '</a>');
		}
		if (current !== "") {
			arrayCurrent = current.split('/');
		}
		$.each(arrayCurrent, function (i,e) {
			if (arrayCurrent.length > 1 && i < arrayCurrent.length-1) {
				$cont_dir.append('<span>/</span><a href="#" class="mioga2-filebrowser-dir-link">' + e + '</a>');
			}
			else {
				$cont_dir.append('<span>/</span><span>' + e + '</span>');
			}
		});
		$cont_dir.parent().data({
			'filebrowser_current': current,
			'filebrowser_root' : root
		});
		$.each($cont_dir.children(), function (i,e) {
			dir_w += parseInt($(e).outerWidth(true));
		});

		if (dir_w > parseInt($this.find('.mioga2-filebrowser-directory-cont').outerWidth(true)) - parseInt($this.find('.mioga2-filebrowser-home-icon').outerWidth(true)) ) {
			$this.find('.mioga2-filebrowser-directory p').css('direction', 'rtl');
		}
		else {
			$this.find('.mioga2-filebrowser-directory p').css('direction', 'ltr');
		}
	}
	/**
	@description Private method drawResize.
				 Method that resizes all elements.

	@method drawResize
	@param Jquery selector of Plugin
	**/
	function drawResize($this) {
		var this_h = parseInt($this.height());
		var this_w = parseInt($this.width());

		var $cont	= $this.find('.mioga2-filebrowser-cont');
		var cont_h	= this_h - parseInt($cont.css('margin-top')) - parseInt($cont.css('margin-bottom'));
		var cont_w	= this_w - parseInt($cont.css('margin-left')) - parseInt($cont.css('margin-right'));
		$cont.height(cont_h + "px");
		$cont.width(cont_w + "px");
		var cont_dir_h	= parseInt($cont.find('.mioga2-filebrowser-directory-cont').height()) + parseInt($cont.find('.mioga2-filebrowser-directory-cont').css('padding-bottom'))  + parseInt($cont.find('.mioga2-filebrowser-directory-cont').css('padding-top'));
		var cont_win_h	= cont_h - cont_dir_h;
		
		var dir_w		= this_w - parseInt($cont.css('margin-left')) - parseInt($cont.css('margin-right')) - parseInt($cont.find('.mioga2-filebrowser-home-icon').outerWidth(true))-4 ;
		$cont.find('.mioga2-filebrowser-directory').width(dir_w + 'px');
		$cont.find('.mioga2-filebrowser-window-cont').height(cont_win_h + "px");
		var cur_root = $this.find('.mioga2-filebrowser-directory').data('filebrowser_root');
		var cur_cur = $this.find('.mioga2-filebrowser-directory').data('filebrowser_current');
		refreshDirectory ($this,cur_root, cur_cur);
	}
	/**
	@description Private method cleanupPath.
				 Method that cleanup string path: remove double slash character and optionaly remove first slash if exists.

	@method cleanupPath
	@param Jquery selector of Plugin
	@param Boolean remove_leading_slash that remove or not a leading slash
	**/
	function cleanupPath (path, remove_leading_slash) {
		if (remove_leading_slash) {
			path = path.replace (/^\//, '');
		}
		return (path.replace (/\/\//g, '/'));
	}
	// ========================================================
	// Methods declaration
	// ========================================================
	/**
	@description Public method that called on plugin initialization.
				 It draws directory block and content folder block.
				 It applies click behavior to directory elements.
	@method init
	@param Object containing plugin options extends with default values of plugin.
	**/
	var methods = {
		init : function(args) {
			var base_obj = $.extend({}, defaults, args);
			return $(this).each(function() {
				var $this = $(this);
				function onResize () {
					drawResize($this);
				}
				if (base_obj.root !== undefined) {
					//format root
					
					if (base_obj.root.charAt(0) === "/") {
						base_obj.root.substr(1,base_obj.root.length-1);
					}
					if (base_obj.root.charAt(base_obj.root.length-1) === "/") {
						base_obj.root = base_obj.root.substr(0,base_obj.root.length-1);
					}
					// format current
					if (base_obj.current.charAt(0) === "/") {
						base_obj.current = base_obj.current.substr(1,base_obj.current.length-1);
					}
					if (base_obj.current.charAt(base_obj.current.length-1) === "/") {
						base_obj.current = base_obj.current.substr(0,base_obj.current.length-2);
					}
					var selected_elem = undefined;
					
					if (base_obj.current !== "" && base_obj.selected === true) {
						if (base_obj.current.indexOf('/') === -1) {
							selected_elem = base_obj.current;
							base_obj.current = "";
						}
						else {
							selected_elem = base_obj.current.substr(base_obj.current.lastIndexOf('/')+1);
							base_obj.current = base_obj.current.substr(0, base_obj.current.lastIndexOf('/')-2);
						}
					}
					$this.data("filebrowser-params", base_obj);
					var $f_manager = $('<div class="mioga2-filebrowser-cont"></div>').append(
						'<div class="mioga2-filebrowser-directory-cont">'
							+ '<span class="mioga2-filebrowser-home-icon"></span>'
							+ '<div class="mioga2-filebrowser-directory"><p></p></div>'
						+ '</div>'
						+ '<div class="mioga2-filebrowser-window-cont">'
							+ '<div class="mioga2-filebrowser-list-cont"><ul class="mioga2-filebrowser-list"></ul></div>'
						+ '</div>'
					);
					$f_manager.appendTo($this);
					// behavior to go at home
					$this.find('.mioga2-filebrowser-home-icon').die('click').live('click', function (ev) {
						refreshList($this, base_obj.root, "", base_obj.change_directory);
					});
					// behavior to click on directory link
					$this.find('.mioga2-filebrowser-directory').find('.mioga2-filebrowser-dir-link').die('click').live('click', function (ev){
						var elemIndex = $this.find('.mioga2-filebrowser-directory').find('.mioga2-filebrowser-dir-link').index($(this));
						var root = $this.find('.mioga2-filebrowser-directory').data('filebrowser_root');
						var current = "";
						$.each($this.find('.mioga2-filebrowser-directory').find('.mioga2-filebrowser-dir-link'), function (i,e) {
							if (i > 0 && i <= elemIndex) {
								if (current === "") {
									current = current + $(this).text();
								}
								else {
									current = current + "/" + $(this).text();
								}
							}
						});
						refreshList($this, root, current, base_obj.change_directory);
						return (false);
					});
					// behavior to click on link folder in list
					$this.find('.mioga2-filebrowser-list .mioga2-filebrowser-dir-link').die('click').live('click',function (ind,elem) {
						var current = $this.find('.mioga2-filebrowser-directory').data('filebrowser_current');
						if (current !== "") {
							current = current + "/" + $(this).text();
						}
						else {
							current = $(this).text();
						}
						refreshList($this, base_obj.root, current, base_obj.change_directory);
						return (false);
					});
					refreshList($this, base_obj.root, base_obj.current, base_obj.change_directory);
					drawResize($this);
					$this.bind('resize', onResize);
				}
				else {
					alert('root must be defined.');
				}
			});
		}
	};
	
	$.fn.fileBrowser = function(method) {
		var args = arguments;
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
		}
		else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, args );
		}
		else {
			$.error( 'Method ' + method + ' does not exist on jQuery.fileBrowser' );
		}
	};
})( jQuery );

/**
@name jquery.tableScrollableSortable
@type jQuery
@author developers@mioga2.org

@requires jQuery v1.7.1
@requires jquery.ba-resize.js
@requires jquery.tablesorter.js v-2.5.2
@requires jquery.tablesorter.widgets.js

style sheet :
				jquery.tableScrollableSortable.css

Copyright 2012, (http://alixen.fr)
The Mioga2 Project. All Rights Reserved.
This Plugin is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

@description This Plugin apply scroll table behavior with fixed headers. It show or hide vertical cursor id needed. If options exists, it apply a sorting behavior to the table.

@param Object
	settings An object literal containing key/value pairs to provide optional settings.
		@option Object scrollable containing key value pair
				@option Integer step to define the count of step for scolling event.
				@option Array field_column that containing objects with key "perc" and value as integer. It is a width proportion versus "width" key.
		@option Object sortable containing key value pair as tablesorter Jquery plugin options.
				It can be contains a table preferences and callback method to save a new preferences, like : 
				@option Object preferences containing preferences of columns table dimensions and default columns of table sorter. It contains key value pair like :
					@option Integer width that is reference to total width of table.
					@option Array sortList that containing array of two integer to defined default columns sorter.
							The first array is a priority column. 
							The first integer is the columns index and second integer is a asc or desc sorting.
					@option Array field_column that containing objects with key "perc" and value as integer. It is a width proportion versus "width" key.
				@option Function storePreferences that is callback to save preferences. That is called when table is resized or sorted.

@example $('div_table_container').tableScrollableSortable({
			scrollable : {
				step : 5
			},
			sortable : {
				parsers: [{
					id: 'priority', 
					is: function(s) { 
						return false; 
					},
					format: function(s) { 
						$.each(that.priority_i18n_index_by_val, function (key, val) {
							if (s.toLowerCase() === val.toLowerCase()) {
								s = key;
							}
						});
						return s; 
					},
					type: 'numeric'
				}],
				sorterOpt : {
					sortList : [ [1,0], [2,1] ],		If this is defined, the sortList parameter from preferences is priority.
					widgets: [ "resizable"] ,
					widgetOptions : { },
					widthFixed: false
				},
				preferences : {
						width : 100,
						sortList : [ [0,0], [1,0] ],
						field_column : [
							{
								label : "dd_1",
								perc : 10
							},
							{
								label : "dd_2",
								perc : 80
							},
							{
								label : "dd_3",
								perc : 10
							}
						]
					},
					storePreferences : function (prefs) {
						console.log(prefs);
					}
			}
	});

	$('#id-cont').userSelect(options);
**/
(function( $ ){
	var defaults = {
		scrollable : {
			step : 3
		},
		sortable : {
			preferences : undefined,
			storePreferences : function (prefs) {
			}
		}
	};
	/**
	@description Private method checkLibraries.
				 Method that checks if necessary libraries are loaded. It returns true or false.

	@method checkLibraries
	@param none
	**/
	function checkLibraries() {
		var result = true;
		return (result);		// TODO find a more clever way to check for dependencies, then remove this line
		if ($('head script[src*="jquery.ba-resize.js"]').length === 0) {
			result = false;
			$.error('jquery.ba-resize.js is required.');
		}
		if ($('head script[src*="jquery.tablesorter.js"]').length === 0) {
			result = false;
			$.error('jquery.tablesorter.js is required.');
		}
		if ($('head script[src*="jquery.tablesorter.widgets.js"]').length === 0) {
			result = false;
			$.error('jquery.tablesorter.widgets.js is required.');
		}
		if ($('head script[src*="jquery.tableScrollableSortable.js"]').length === 0) {
			result = false;
			$.error('jquery.tableScrollableSortable.js is required.');
		}
		if ($('head link[href*="jquery.tableScrollableSortable.css"]').length === 0) {
			result = false;
			$.error('jquery.tableScrollableSortable.css is required.');
		}
		return result;
	}
	/**
	@description Private method cursorAt.
				 Method that modify cursor position according to proportion of table visible elements.

	@method cursorAt
	@param Jquery selector of plugin
	**/
	function cursorAt($that) {
		var options = $that.data('plug-opt');
		var perc = options.first_visible_elm * 100 / options.elem_not_vis;
		$that.find('.mioga2-scr-pan-cursor').css({"top" : perc* options.h_restante/100 + "px"});
	}
	/**
	@description Private method scrollAt.
				 Method that show table elements according to index of first visible element defined in Jquery data key.

	@method scrollAt
	@param Jquery selector of plugin
	**/
	function scrollAt ($that) {
		var options = $that.data('plug-opt');
		$that.find('.mioga2-scr-active').removeClass('mioga2-scr-active');
		for (var i=options.first_visible_elm; i < options.first_visible_elm + options.count_visible_elm; i++) {
			$that.find('.mioga2-scr-item').eq(i).addClass('mioga2-scr-active');
		}
	}
	/**
	@description Private method tableRefresh.
				 Method that refresh content of table. For to do this, it removes all classes of elements and resizes content table.
				 It shows pan cursor if needed, scroll table at last visible element and adjust cursor position.
				 It apply wheel scrolling behavior if needed or detach mouse wheel event.

	@method tableRefresh
	@param Jquery selector of plugin
	@param Object options containing plugin options
	@param Function cb that is called if defined at end of tableRefresh method.
	**/
	function tableRefresh ($that, options, cb) {
		function detachMouseWheelEvent() {
			options.context.onmousewheel = null;
			if (options.context.addEventListener) {
				if (options.context.removeEventListener) {
					options.context.removeEventListener('DOMMouseScroll', wheel, false);
				}
			}
		}
		$that.find('.mioga2-scr-active, .mioga2-scr-item, .mioga2-scr-header').removeClass('mioga2-scr-active').removeClass('mioga2-scr-item').removeClass('mioga2-scr-header');
		var h_this		= parseFloat($that.height()) - parseFloat($that.css('paddingTop')) - parseFloat($that.css('paddingBottom'));
		var h_header 	= 0;
		var h_cont		= 0;
		var h_total		= 0;
		var h_item		= 0;

		$that.data('plug-opt').count_elm 	= 0;
		$that.data('plug-opt').count_visible_elm = 0;
		$that.data('plug-opt').elem_not_vis = 0;
		var h_cursor_cont = 0;
		var w_cursor = 0;
		$that.data('plug-opt').h_cursor = 0;
		$that.data('plug-opt').h_restante = 0;
		$that.data('plug-opt').cursor_step = 0;
		$that.data('plug-opt').min_offset = 0;
		$that.data('plug-opt').max_offset = 0;
		$.each($that.find('tr'), function (i,e) {
			if ($(e).find('th').length) {
				$(e).addClass('mioga2-scr-header');
				h_header = parseFloat($(e).outerHeight( true )); 
			}
			else {
				h_total += parseFloat($(e).outerHeight( true ));
				$(e).addClass('mioga2-scr-item');
				$that.data('plug-opt').count_elm ++;
			}
		});

		h_cont 	= h_this - h_header;
		h_item	= h_total/$that.data('plug-opt').count_elm;
		if (h_cont/h_item <  $that.data('plug-opt').count_elm) {
			$that.data('plug-opt').count_visible_elm = parseInt(h_cont/h_item);
		}
		else {
			$that.data('plug-opt').count_visible_elm = $that.data('plug-opt').count_elm;
		}
		$that.data('plug-opt').elem_not_vis = $that.data('plug-opt').count_elm - $that.data('plug-opt').count_visible_elm;
		if ($that.data('plug-opt').elem_not_vis < $that.data('plug-opt').first_visible_elm) {
			$that.data('plug-opt').first_visible_elm = $that.data('plug-opt').elem_not_vis; 
		}
		scrollAt ($that);
		var $pan = $that.find('.mioga2-scr-pan-cont');
		$pan.show();
		if ($that.data('plug-opt').count_visible_elm < $that.data('plug-opt').count_elm) {
			$that.find('.mioga2-scr-pan-cont').height($that.data('plug-opt').count_visible_elm * h_item+ 'px');
			h_cursor_cont = ($that.data('plug-opt').count_visible_elm * h_item ) - $that.find('.mioga2-scr-pan-top').outerHeight(true) - $that.find('.mioga2-scr-pan-bottom').outerHeight(true);
			w_cursor = parseFloat($pan.outerWidth(true));
			$that.find('.mioga2-scr-pan-cursor-cont').height(h_cursor_cont  + 'px');

			if ( $that.data('plug-opt').count_visible_elm*h_cursor_cont/$that.data('plug-opt').count_elm > 15) {
				$that.data('plug-opt').h_cursor = $that.data('plug-opt').count_visible_elm*h_cursor_cont/$that.data('plug-opt').count_elm;
			}
			else {
				$that.data('plug-opt').h_cursor = 15;
			}
			$that.data('plug-opt').h_restante = h_cursor_cont - $that.data('plug-opt').h_cursor;
			$that.find('.mioga2-scr-pan-cursor').height( $that.data('plug-opt').h_cursor + 'px');

			$pan.css({"position" : "relative", "top" : h_header + "px"});

			$that.data('plug-opt').cursor_step = $that.data('plug-opt').h_restante/$that.data('plug-opt').elem_not_vis;
			$that.data('plug-opt').min_offset = $that.find('.mioga2-scr-pan-cursor-cont').offset().top;
			$that.data('plug-opt').max_offset = $that.data('plug-opt').min_offset + $that.data('plug-opt').h_restante;

			cursorAt($that);
			$pan.find('.mioga2-scr-pan-bottom').unbind('click').click(function (ev) {
				var current_first_elm = $that.data('plug-opt').first_visible_elm; 
				for ( var i=0; i < $that.data('plug-opt').step; i++ ) {
					if ($that.data('plug-opt').first_visible_elm+1 <= $that.data('plug-opt').count_elm - $that.data('plug-opt').count_visible_elm) {
						$that.data('plug-opt').first_visible_elm ++;
					}
					else {
						break;
					}
				}
				if ($that.data('plug-opt').first_visible_elm > current_first_elm) {
					scrollAt($that);
					cursorAt($that);
				}
			});
			$pan.find('.mioga2-scr-pan-top').unbind('click').click(function (ev) {
				var data = $that.data('plug-opt');
				var current_first_elm = $that.data('plug-opt').first_visible_elm;
				for ( var i=0; i < $that.data('plug-opt').step; i++ ) {
					if ($that.data('plug-opt').first_visible_elm - 1 >= 0 ) {
						$that.data('plug-opt').first_visible_elm --;
					}
					else {
						break;
					}
				}
				if ($that.data('plug-opt').first_visible_elm < current_first_elm) {
					scrollAt($that);
					cursorAt($that);
				}
			});
			$pan.find('.mioga2-scr-pan-cursor-cont').unbind('click').click(function (ev) {
				var cursor_top = $(this).children().offset().top;
				if (ev.pageY < cursor_top) {
					var current_first_elm = $that.data('plug-opt').first_visible_elm;
					for ( var i=0; i < $that.data('plug-opt').count_visible_elm; i++ ) {
						if ($that.data('plug-opt').first_visible_elm - 1 >= 0 ) {
							$that.data('plug-opt').first_visible_elm --;
						}
					}
					if ($that.data('plug-opt').first_visible_elm < current_first_elm) {
						scrollAt($that);
						cursorAt($that);
					}
				}
				else if (ev.pageY > cursor_top + $that.data('plug-opt').h_cursor){
					var current_first_elm = $that.data('plug-opt').first_visible_elm; 
					for ( var i=0; i < $that.data('plug-opt').count_visible_elm; i++ ) {
						if ($that.data('plug-opt').first_visible_elm + 1 <= $that.data('plug-opt').count_elm - $that.data('plug-opt').count_visible_elm) {
							$that.data('plug-opt').first_visible_elm ++;
						}
					}
					if ($that.data('plug-opt').first_visible_elm > current_first_elm) {
						scrollAt($that);
						cursorAt($that);
					}
				}
			});
			detachMouseWheelEvent();
			if (options.context.addEventListener) {
				if (!options.context.addEventListener.DOMMouseScroll) {
					options.context.addEventListener('DOMMouseScroll', wheel, false);
				}
			}
			if (options.context.onmousewheel === undefined) {
				options.context.onmousewheel = wheel;
			}
		}
		else {
			$pan.hide();
			detachMouseWheelEvent();
		}
		$that.find('.mioga2-scr-table-cont').width(parseFloat($that.outerWidth(true)) - parseFloat($that.css('paddingLeft')) - parseFloat($that.css('paddingRight')) - w_cursor - 4 + "px");
		if (cb) {
			cb();
		}
	}
	/**
	@description Private method setPreferences.
				 Method that call store function from Plugin options with new preferences.

	@method setPreferences
	@param Object that containing new preferences. see Plugin options at the beginning of this page.
	@param Function store defined in Plugin options.
	**/
	function setPreferences (pref, store) {
		if (store !== undefined) {
			store(pref);
		}
	}
	/**
	@description Private method wheel.
				 Method that creates wheel event attached to plugin selector for scrolling behavior.

	@method wheel
	@param Object event that is scroll event by mouse.
	@param Jquery selector of the Plugin
	**/
	function wheel(event, $this){
		var $this = $(this);
		var delta = 0;
		if (!event)
			event = window.event;
		if (event.wheelDelta) {
			delta = event.wheelDelta/120;
		} else if (event.detail) {
			delta = -event.detail/3;
		}
		if (delta) {
			handle(delta, $this);
		}
		if (event.preventDefault) {
			event.preventDefault();
		}
		event.returnValue = false;
	}
	/**
	@description Private method handle.
				 Method that trigger click event to cursor button according to type of mouse wheel event: up or down.

	@method handle
	@param Integer delta
	@param Jquery selector of the Plugin
	**/
	function handle(delta, $this) {
		if (delta < 0 ) {
			$this.find('.mioga2-scr-pan-bottom').trigger('click');
		}
		else {
			$this.find('.mioga2-scr-pan-top').trigger('click');
		}
	}
	/**
	@description Public method that called on plugin initialization.
				 It makes plugin options extended with default values.
				 It draws HTML elements and associated it the resize behavior.
				 It defined the mouse drag and drop cursor behavior.
	@method init
	@param Object containing plugin options.
	**/
	var methods = {
		init : function(args) {
			if (checkLibraries() === true) {
				var options = {};
				options.scrollable = $.extend(defaults.scrollable, args.scrollable);
				options.sortable = $.extend(defaults.sortable, args.sortable );

				if (options.sortable.preferences) {
					if (options.sortable.preferences.sortList.length > 0) {
						if (options.sortable.sorterOpt) {
							options.sortable.sorterOpt.sortList = options.sortable.preferences.sortList;
						}
					}
				}

				return $(this).each(function() {
					var $this 	= $(this);
					var that = this;
					that.options = options;
					that.options.context = that;
					$this.data('plug-opt', {
						count_sorting : 0,
						first_visible_elm : 0,
						current_top : 0,
						count_elm : 0,
						count_visible_elm : 0,
						elem_not_vis : 0,
						h_cursor : 0,
						h_restante : 0,
						cursor_step : 0,
						min_offset : 0,
						max_offset : 0,
						step : that.options.scrollable.step
					});
					if (that.options.sortable.parsers) {
						$.each(that.options.sortable.parsers, function (ind, parser) {
							$.tablesorter.addParser(parser);
						});
					}
					function onResize () {
						$this.find('table').trigger("update");
						tableRefresh($this, that.options);
					}
					$this.data('plug-opt').HTMLTable = $this.html();
					$this.addClass('mioga2-scr-container');
					$this.find('table').wrap('<div class="mioga2-scr-table-cont"></div>');
					$(
						'<div class="mioga2-scr-pan-cont">' + 
							'<span class="mioga2-scr-pan-top"></span>' + 
							'<div class="mioga2-scr-pan-cursor-cont">' + 
								'<span class="mioga2-scr-pan-cursor draggable"></span>' + 
							'</div>' + 
							'<span class="mioga2-scr-pan-bottom"></span>' + 
						'</div>'
					).appendTo($this);
					tableRefresh($this, that.options, function () {
						if (that.options.sortable.sorterOpt) {
							$this.find('th').mouseup(function (ev) {
								var glob = 0;
								$.each($this.find('th'), function (i,e) {
									glob += parseInt($this.find('table th').eq(i).outerWidth(true));
								});
								that.options.sortable.preferences = {
									width : glob,
									sortList : $this.find('table').data('tablesorter').sortList,
									field_column : []
								}; 
								$.each($this.find('th'), function (i,e) {
									that.options.sortable.preferences.field_column.push({
										label : $(e).find('.tablesorter-header-inner').text(),
										perc : parseInt($(e).outerWidth(true))*that.options.sortable.preferences.width/glob
									});
								});
								setPreferences (that.options.sortable.preferences, that.options.sortable.storePreferences);
							});
							$this.find('table').bind('sortEnd', function () {
								scrollAt($this);
							});
							$this.find('table').tablesorter(that.options.sortable.sorterOpt);
						}
						if (that.options.sortable.preferences !== undefined) {
							var glob = 0;
							$.each($this.find('th'), function (i,e) {
								glob += parseInt($this.find('table th').eq(i).outerWidth(true));
							});
							$.each(that.options.sortable.preferences.field_column, function (i,e) {
								$this.find('table th').eq(i).width( (e.perc*glob/that.options.sortable.preferences.width) + "px");
							});
						}
						$this.bind('resize',onResize);
						$this.find('.mioga2-scr-table-cont tbody').bind('resize',onResize);
						
					});

					var mouseDownY = 0;
					var clonedElm = null;
					$this.mousedown(onMouseDown);
					$this.mousemove(onMouseMove);
					$this.mouseup(onMouseUp);

					function onMouseDown (event) {
						if ($(event.target).hasClass('mioga2-scr-pan-cursor')) {
							$this.data('plug-opt').current_top = parseFloat($this.find('.mioga2-scr-pan-cursor').offset().top);
						}
						if (event.srcElement == undefined && event.target != undefined) {
							event.srcElement = event.target;
						}
						if (!$(event.srcElement).hasClass("draggable")) {
							return;
						}
						mouseDown = true;
						clonedElm = $(event.srcElement);
						mouseDownY = event.pageY;

						return false;
					}

					function onMouseMove (event) {
						if (clonedElm == null) {
							return;
						}
						var cloneOffset = $(clonedElm).offset();
						var y = cloneOffset.top + (event.pageY - mouseDownY);
						if (y <= $this.data('plug-opt').max_offset && y - $this.data('plug-opt').min_offset >= 0) {
							if (Math.abs(y - $this.data('plug-opt').current_top) >= $this.data('plug-opt').cursor_step) {
								var perc = (y - $this.data('plug-opt').min_offset)*100/$this.data('plug-opt').h_restante;
								var new_elm = perc * $this.data('plug-opt').elem_not_vis / 100;
								if (new_elm - parseInt(new_elm) < 1/2) {
									$this.data('plug-opt').first_visible_elm = parseInt(new_elm);
								}
								else {
									$this.data('plug-opt').first_visible_elm = parseInt(new_elm)+1;
								}
								scrollAt ($this, $this.data('plug-opt'));
								$this.data('plug-opt').current_top = y;
							}
							else if ($this.data('plug-opt').max_offset - y < $this.data('plug-opt').cursor_step && $this.data('plug-opt').max_offset > $this.data('plug-opt').current_top) {
								$this.data('plug-opt').first_visible_elm = $this.data('plug-opt').elem_not_vis;
								scrollAt ($this, $this.data('plug-opt'));
								y = $this.data('plug-opt').max_offset;
								$this.data('plug-opt').current_top = y;
							}
							else if (y - $this.data('plug-opt').min_offset < $this.data('plug-opt').cursor_step && $this.data('plug-opt').min_offset < $this.data('plug-opt').current_top) {
								$this.data('plug-opt').first_visible_elm = 0;
								scrollAt ($this, $this.data('plug-opt'));
								y = $this.data('plug-opt').min_offset;
								$this.data('plug-opt').current_top = y;
							}
							$(clonedElm).offset({top:y});
						}
						mouseDownY = event.pageY;
						return false;
					}

					function onMouseUp (event)  {
						mouseDown = false;
						clonedElm = null;
						mouseDownY = 0;
					}
				});
			}
		}
	};
	/**
	@namespace $.fn.tableScrollableSortable
	**/
	$.fn.tableScrollableSortable = function(method) {
		var args = arguments;
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
		}
		else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, args );
		}
		else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.tableScrollableSortable' );
		}
	};
})( jQuery );

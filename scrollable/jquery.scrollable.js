/**
@name jquery.scrollable
@type jQuery
@author developers@mioga2.org

@requires jQuery v1.7.1
@requires jquery.ba-resize.js

style sheet :
				jquery.scrollable.css

Copyright 2012, (http://alixen.fr)
The Mioga2 Project. All Rights Reserved.
This Plugin is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

@description This Plugin apply scroll event to HTML element. It draws up and down buttons and apply mouse wheel event to scroll content.

@param Object
	settings An object literal containing key/value pairs to provide optional settings.
		@option integer step that defined count step by scroll event.

@example $('#scrollable-content').scrollable({
		step : 5
	});
**/
(function( $ ){
	var defaults = {
		step : 3
	};
	// ========================================================
	// Privates Methods
	// ========================================================
	/**
	@description Private method checkLibraries.
				 Method that checks if necessary libraries are loaded. It returns true or false.

	@method checkLibraries
	@param none
	**/
	function checkLibraries() {
		var result = true;
		if ($('head script[src*="jquery.ba-resize.js"]').length === 0) {
			result = false;
			$.error('jquery.ba-resize.js is required.');
		}
		if ($('head link[href*="jquery.scrollable.css"]').length === 0) {
			result = false;
			$.error('jquery.scrollable.css is required.');
		}
		return result;
	}
	/**
	@description This function that apply scroll down event
	@method scrollDown
	@param Jquery selector of Plugin.
	@param Integer step
	**/
	function scrollDown ($elem, step) {
		// While the bottom of container is above bottom of scrollable data block, to scroll
		var cont_y = $elem.find('.mioga2-scr-content').offset().top + parseInt($elem.find('.mioga2-scr-content').css('height'));
		var data_y = parseInt($elem.find('.mioga2-scr-data').css('height')) + $elem.find('.mioga2-scr-data').offset().top;
		for (var i=0; i < step; i++) {
			for (var j=0; j< 3; j++) {
				if (cont_y < data_y) {
					var top = parseInt( $elem.find('.mioga2-scr-data').css('top') );
					$elem.find('.mioga2-scr-data').css('top', top - 1 + "px");
					data_y --;
				}
			}
		}
	}
	/**
	@description This function that apply scroll up event
	@method scrollUp
	@param Jquery selector of Plugin.
	@param Integer step
	**/
	function scrollUp ($elem, step) {
		// While the top of the container is above top of scrollable data block, to scroll
		for (var i=0; i < step; i++) {
			for (var j=0; j< 3; j++) {
				if ($elem.find('.mioga2-scr-content').offset().top >= $elem.find('.mioga2-scr-data').offset().top) {
					var top = parseInt( $elem.find('.mioga2-scr-data').css('top') );
					$elem.find('.mioga2-scr-data').css('top', top + 1 + "px");
				}
			}
		}
	}
	// ========================================================
	// Methods declaration
	// ========================================================
	var methods = {
		/**
		@description Public method that called on Plugin initialization.
					 It wraps HTML content and draw up and down buttons.
					 It applies resize behavior that detaches mouse wheel event and apply mouse wheel event if needed.
					 It applies click behavior to up and down buttons.

		@method init
		@param Object containing Plugin options extends with default values of Plugin.
		**/
		init : function(args) {
			if (checkLibraries() === true) {
				var options = $.extend({}, defaults, args);
				return $(this).each(function() {
					var $this	= $(this);
					var that	= this;
					var $data	= $this.children().detach();
					var glob_h	= parseInt($this.outerHeight(true));
					$this.append(
						'<div class="mioga2-scr-container">' +
							'<div class="mioga2-scr-btn-prev ui-icon ui-icon-triangle-1-n"><span>&nbsp;</span></div>' +
							'<div class="mioga2-scr-content">' +
								'<div class="mioga2-scr-data"></div>' +
							'</div>' +
							'<div class="mioga2-scr-btn-next ui-icon ui-icon-triangle-1-s"><span>&nbsp;</span></div>' +
						'</div>'
					);
					$this.find('.mioga2-scr-data').append($data).css({
						top : 0
					});
					$this.find('.mioga2-scr-btn-prev').click(function () {
						scrollUp($this, options.step);
					});
					$this.find('.mioga2-scr-btn-next').click(function () {
						scrollDown($this, options.step);
					});
					$this.find('.mioga2-scr-content').height(
						$this.find('.mioga2-scr-container').outerHeight(true) - $this.find('.mioga2-scr-btn-prev').outerHeight(true) - $this.find('.mioga2-scr-btn-next').outerHeight(true)
					);
					function onResize () {
						that.onmousewheel = null;
						if (that.addEventListener) {
							if (that.removeEventListener) {
								console.log('remove mousewheel event');
								that.removeEventListener('DOMMouseScroll', wheel, false);
							}
						}
	
						if (glob_h > parseInt($this.find('.mioga2-scr-data').height())) {
							console.log('pas besoin');
							$this.find('[class*="mioga2-scr-btn-"]').removeClass('mioga2-scr-show').addClass('mioga2-scr-hide');
							$this.find('.mioga2-scr-content').height(glob_h + 'px');
							$this.find('.mioga2-scr-data').offset({'top': $this.find('.mioga2-scr-container').offset().top} );
						}
						else {
							console.log('besoin');
							$this.find('[class*="mioga2-scr-btn-"]').removeClass('mioga2-scr-hide').addClass('mioga2-scr-show');
							$this.find('.mioga2-scr-content').height(glob_h- parseInt($this.find('.mioga2-scr-btn-prev').height()) - parseInt($this.find('.mioga2-scr-btn-next').height()) + 'px');
							
							var data_h 		= parseInt($this.find('.mioga2-scr-data').outerHeight(true));
							var data_top 	= $this.find('.mioga2-scr-data').offset().top;
							var data_y 		= data_h + data_top;
							var content_h 	= parseInt($this.find('.mioga2-scr-content').outerHeight(true));
							var content_y 	= content_h + $this.find('.mioga2-scr-content').offset().top;
							if (data_h > content_h) {
								while (data_y < content_y) {
									$this.find('.mioga2-scr-data').offset({top : data_top+1});
									data_y ++;
									data_top ++;
								}
							}
							if (that.addEventListener) {
								if (!that.addEventListener.DOMMouseScroll) {
									// DOMMouseScroll is for mozilla
									that.addEventListener('DOMMouseScroll', wheel, false);
								}
							}
							//IE/Opera
							that.onmousewheel = wheel;
						}
						return true;
					}
					var resized = $this.find('.mioga2-scr-data').bind('resize', onResize);
					if (resized !== true ) {
						onResize ();
					}
					// Event handler for mouse wheel event.
					function wheel(event){
						console.log('event');
						console.log(event);
						console.log('this : ');
						console.log(this);
						var delta = 0;
						if (!event) /* For IE. */
							event = window.event;
						if (event.wheelDelta) { /* IE/Opera. */
							delta = event.wheelDelta/120;
						} else if (event.detail) { /* Mozilla case. */
							// In Mozilla, sign of delta is different than in IE.
							// Also, delta is multiple of 3.
							delta = -event.detail/3;
						}
						// If delta is nonzero, handle it.
						// Basically, delta is now positive if wheel was scrolled up,
						// and negative, if wheel was scrolled down.
						if (delta) {
							handle(delta);
						// Prevent default actions caused by mouse wheel.
						// That might be ugly, but we handle scrolls somehow
						// anyway, so don't bother here..
						}
						if (event.preventDefault) {
							event.preventDefault();
						}
						event.returnValue = false;
					}
					// This is high-level function.
					// It must react to delta being more/less than zero.
					function handle(delta) {
						if (delta < 0 ) {
							scrollDown($this, options.step);
						}
						else {
							scrollUp($this, options.step);
						}
					}
				});
			}
		}
	};
	/**
	@namespace $.fn.scrollable
	**/
	$.fn.scrollable = function(method) {
		var args = arguments;
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
		}
		else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, args );
		}
		else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.scrollable' );
		}
	};
})( jQuery );
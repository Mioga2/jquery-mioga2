// ========================================================
// jquery.inlineMessage.js
// A inline-message generator
// Usage :
//     <div id="cont-id">
//         ...
//     </div>
//
//     $('#cont-id').inlineMessage ();
//     $('#cont-id').inlineMessage ('info', 'Message to display');
//     $('#cont-id').inlineMessage ('error', 'Message to display');
// ========================================================
(function ($) {
	$.fn.inlineMessage = function (options) {
		var that = this;

		var methods = {
			// "info" displays an info message and automatically closes it after a few seconds
			info: function (message) {
				$(that).find ('p.error').hide ();
				$(that).find ('p.info').empty ().append (message).show ();
				$(that).show ().delay (5000).fadeOut ();
			},
			// "error" displays an error message that will not be closed automatically
			error: function (message) {
				$(that).find ('p.info').hide ();
				$(that).find ('p.error').empty ().append (message).show ();
				$(that).show ();
			}
		};

		if (methods[options]) {
			return methods[options].apply (this, Array.prototype.slice.call (arguments, 1));
		}
		else {
			this.each (function (i, item) {
				var $elem = $(item);

				// Set element class and append message containers
				$elem.addClass ('inline-message').append ('<p class="error"></p>').append ('<p class="info"></p>').hide ();

				// Add close icon
				$('<div class="close"></div>').appendTo ($elem).click (function () {
					$elem.fadeOut ();
				});
			});
		}

		return (this);
	};
}) (jQuery);

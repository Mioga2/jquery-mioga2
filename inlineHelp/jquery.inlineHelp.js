// ========================================================
// jquery.inlineHelp.js
// A inline help plugin
// Usage :
//
//     $('<div></div>').inlineHelp ({
//         context: <mioga_context object>,
//         application: <application_name>,
//         anchor: <anchor_name>
//     });
// ========================================================
(function ($) {
	$.fn.inlineHelp = function (params) {
		this.each (function (i, item) {
			var $elem = $(item);

			// Set main class
			$elem.addClass ('inline-help');

			// Add toggle button and help container
			var $panel = $('<div class="inline-help-panel"></div>');
			var $cont = $('<div class="inline-help-contents"></div>').appendTo ($panel);
			var $toggle = $('<div class="inline-help-toggle"></div>').appendTo ($elem).click (function () {
				if ($cont.html () === '') {
					var url = params.context.help_uri + '/' + params.context.user.lang + '/' + params.application + '.html';
					// Fetch help page
					$.ajax ({
						url: url,
						type: 'GET',
						success: function (data) {
							console.log ("[jquery.inlineHelp] Got help contents.");
							$cont.append (data);

							// Process links
							$.each ($cont.find ('a'), function (index, elem) {
								var href = $(elem).attr ('href');
								if (href) {
									if (href.match (/^#/)) {
										// Link points to an anchor in current page, add full URL
										$(elem).attr ('href', url + href);
									}
									else if (href.match (/^\//)) {
										// Link points to absolute URL, nothing to be done
									}
									else if (href.match (/^http/)) {
										// Link points to absolute URL with protocol information, nothing to be done
									}
									else {
										// Links points to relative URL, rewrite URL
										$(elem).attr ('href', url.replace (/[^\/]*$/, '') + href);
									}
									$(elem).attr ('target', '_blank');
								}
							});
						},
						error: function (data) {
							$cont.append ('<div class="error"><div>Help page not found !</div></div>');
						}
					});
				}

				// Show panel
				$panel.animate ({width: 'toggle'}, function () {
					$toggle.toggleClass ('open');
					if ($('#' + params.anchor).length) {
						$('#' + params.anchor).get (0).scrollIntoView ();
					}
				});
			});
			$panel.appendTo ($elem).hide ();

		});

		return (this);
	};
}) (jQuery);

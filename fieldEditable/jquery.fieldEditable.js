/**
@name jquery.fieldEditable
@type jQuery
@author developers@mioga2.org

@requires jQuery v1.7.1
style sheet jquery.fieldEditable.css

Copyright 2012, (http://alixen.fr)
The Mioga2 Project. All Rights Reserved.
This Plugin is free software; you can redistribute it and/or
modify it under the terms of the GNU Public License.

@description fieldEditable is a Plugin that edits a field.
	For to do this, plugin selector is a container of field value will be editable.
	It associates to this selector, a behavior click that hides the selector and draws input field with current field value.
	The validation and cancellation can be called by click on specific icon or by specific key press.
	On the validation, a test is made that check if the new value is not empty and called checkField method for to check the field value.
	A callback is called if checkField method return true and if onValidate callback method is defined in Plugin options.

@example $('.fields').fieldEditable( {
			onValidate : function ($old_cont, new_value, old_value) {
				if (new_value === old_value ) {
					$old_cont.next('p').remove();
					$old_cont
						.css( 'background-color' , 'red' )
						.after('<p>The new value : ' + new_value + ' is the same as old value : ' + old_value + '</p>');
				}
				else {
					$old_cont.next('p').remove();
					$old_cont.css( 'background' , 'none' )
				}
			},
			checkField : function ($old_cont,new_value) {
				console.log(new_value);
				var result = true;
				if (new_value.length < 7 ) {
					result = false;
				}
				return result;
			},
			onError : function ($old_cont, $field_cont, new_value) {
			alert ('Must be more characters please');
			}
		});
**/

(function( $ ){
	/**
	@param Object
			settings An object literal containing key/value pairs to provide
			optional settings.
	**/
	var defaults = {
		/**
		@description Callback method that is called on validate field edition and if checkField method return true. 

		@method onValidate
		@param {Object} $old_cont A Jquery object that is initial selector of Plugin
		@param {String} new_value A text as HTML format that is a new field value
		@param {String} old_value A text as HTML format that is a old field value
		@optional
		@default undefined
		**/
		onValidate : undefined,
		/**
		@description Callback method that is called on validate field edition. It checks field validity. 

		@method checkField
		@param {Object} $old_cont A Jquery object that is initial selector of Plugin
		@param {String} new_value A text as HTML format that is a new field value
		@return {Boolean}
		@optional
		@default true
		**/
		checkField : function ($old_cont, new_value) {
			return true;
		},
		/**
		@description Method that is called on validate field edition and if checkField method return false. 

		@method onError
		@param {Object} $old_cont A Jquery object that is initial selector of Plugin
		@param {Object} $field_cont A Jquery object that is field container
		@param {String} new_value A text as HTML format that is a new field value
		@optional
		@default undefined
		**/
		onError : undefined
	};
	/**
	@description Private method checkLibraries.
				 Method that checks if necessary libraries are loaded. It returns true or false.

	@method checkLibraries
	@param none
	**/
	function checkLibraries() {
		var result = true;
		return result;
	}
	/**
	@description Method that apply an error CSS class to field.

	@method drawError
	@param {Object} $field A Jquery object that is field
	**/	
	function drawError ($field) {
		if ($field.hasClass('mioga2-fieldEditable-field-error') === false) {
			$field.addClass('mioga2-fieldEditable-field-error');
		}
	}
	/**
	@description Private method validate.
		It replaces field edition container by the plugin selector with modify value.
		If callback cb is defined in parameters methods, it is called.

	@method validate
	@param {Object} $field_cont A Jquery object that is field container
	@param {Object} $old_cont A Jquery object that is input field
	@param {String} new_value A text as HTML format that is a new field value
	@param {Function} checkField A function that check a new value. 
	@param {Function} onError A callback function that is called if checkField method return is false
	@param {Function} cb A callback function that is called if checkField is true and after the redraw old field with the new value.
	**/
	function validate ($field_cont, $old_cont, new_value, checkField, onError, onValidate) {
		if (checkField($old_cont, new_value) === true) {
			var old_value = $old_cont.html();
			$old_cont.html(new_value);
			$field_cont.remove();
			$old_cont.show();
			if (onValidate) {
				onValidate($old_cont, new_value, old_value);
			}
		}
		else if (onError) {
			drawError ($field_cont.find('.mioga2-fieldEditable-field'));
			if (onError) {
				onError ($old_cont, $field_cont, new_value);
			}
		}
		else {
			drawError ($field_cont.find('.mioga2-fieldEditable-field'));
		}
	}
	// ========================================================
	// Methods declaration
	// ========================================================
	
	var methods = {
		/**
		@description initialization of fieldEditable Plugin

		@method init
		@chainable
		@param {Object} args A plugin options
		@optional
		@param {Function} args.onValidate A callback function of validation the field edition
		@optional
		@extends default
		**/
		init : function(args) {
			if (checkLibraries() === true) {
				var base_obj = $.extend({}, defaults, args);
				return $(this).each(function() {
					var $old_cont 	= $(this);
					// behavior to activates a field edition
					$old_cont.live('click', function (ev) {
						var $field_cont = $('<div class="mioga2-fieldEditable-field-cont"></div>');
						$old_cont.after($field_cont).hide();
						var $field = $('<input type="text" value="' + $old_cont.html() + '" class="mioga2-fieldEditable-field"/>');
						// keyup behavior to input field
						$field.keyup(function (ev) {
							if (ev.keyCode) {
								if (ev.keyCode == 13) {
									var new_value = $.trim($field.val());
									validate($field_cont, $old_cont, new_value, base_obj.checkField, base_obj.onError, base_obj.onValidate);
								}
								else if (ev.keyCode == 27) {
									$field_cont.remove();
									$old_cont.show();
								}
								else {
									if ($(this).hasClass('mioga2-fieldEditable-field-error') === true) {
										$(this).removeClass('mioga2-fieldEditable-field-error');
									}
								}
							}
						}).appendTo($field_cont);
						// validate button
						$('<span class="mioga2-fieldEditable-validate-btn"></span>').click(function (ev) {
							var new_value = $.trim($field.val());
							validate($field_cont, $old_cont, new_value, base_obj.checkField, base_obj.onError, base_obj.onValidate);
						}).appendTo($field_cont);
						// cancel button
						$('<span class="mioga2-fieldEditable-cancel-btn"></span>').click(function (ev) {
							$field_cont.remove();
							$old_cont.show();
						}).appendTo($field_cont);
						$field.focus();
					});
				});
			}
		}
	};

	/**
	@namespace $.fn.fieldEditable
	**/
	$.fn.fieldEditable = function(method) {
		var args = arguments;
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( args, 1 ));
		}
		else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, args );
		}
		else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.fieldEditable' );
		}
	};	
})( jQuery );